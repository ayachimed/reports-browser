app.service("Files",["$http",function ($http) {
	$http.defaults.headers.common.Authorization = 'Bearer TOKEN_DEV_CFNeVo71dgF0l4WV8wffK8XcXe5565U1YpfMD9hiCFMmconxMLHUpM';
	return{
		list:function (data) {
			/*return $http.post("https://api.0-0.online/public/data/1/file.list",data,);*/
			
			return $http({
			    method: 'POST',
			    url: 'https://api.0-0.online/public/data/1/file.list',
			    data: data,
			    headers:{
			    	"Content-Type":"application/x-www-form-urlencoded"
			    }
			});
		},
		download:function (fileid) {
			return $http({
			    method: 'POST',
			    url: 'https://api.0-0.online/public/data/1/file.get',
			    data: 'fileid='+fileid,
			    headers:{
			    	"Content-Type":"application/x-www-form-urlencoded"
			    },
			    responseType: 'arraybuffer'
			});
		},
		checkAccount:function (deviceID) {
			return $http({
				method:'POST',
				url:"https://api.0-0.online/public/user/1/account.login",
				data:"deviceid="+deviceID,
				headers:{
			    	"Content-Type":"application/x-www-form-urlencoded"
			    },
			});
		},
		login:function (data) {
			return $http({
				method:'POST',
				url:"https://api.0-0.online/public/user/1/account.login",
				data:"deviceid="+data.deviceID+"&password="+data.password,
				headers:{
			    	"Content-Type":"application/x-www-form-urlencoded"
			    },
			});
		},
	}
}]);