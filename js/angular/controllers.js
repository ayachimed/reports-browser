app.controller("appCtrl",["$scope","$http","Files",function ($scope,$http,Files) {
	
	$scope.modalOptions = {backdrop: 'static', keyboard: false};
	$scope.files = [];
	$scope.selectedIndex = -1;
	$scope.selectedFile = null;
	$scope.filesLoaded = false;
	$scope.imageSource = "";
	$scope.isLoading = false;
	$scope.isImage = false;
	$scope.fileData = null;
	$scope.valid_input = false;
	$scope.deviceID="";
	
	$scope.sortType="Filename";
	$scope.sortReverse = false;

	$scope.passwordPopup="Please enter your existing password";
	$scope.newUser={};
	$scope.error=false;

	$scope.viewFile = function (file) {
		$scope.selectedFile = file;
		$("#fileModal").modal("show");
		$scope.isLoading = true;
		Files.download($scope.selectedFile.FileID)
			.success(function (data) {
				$scope.isLoading = false; 
				if($scope.selectedFile.MIMEType.toLowerCase() == "application/pdf")
					$scope.viewPdf(data);
				else
					$scope.viewImage(data);
			})
			.error(function () {
				console.error("error");
			});
	}
	$scope.prompt = function () {
		$("#promptModal").modal($scope.modalOptions)
	}
	$scope.checkAccount = function () {
		//$scope.valid_input = true;
		if($scope.deviceID.toString().trim() != ""){
			Files.checkAccount($scope.deviceID)
			.success(function (data) {
				//check account status
				
				if(data.success){
					if(data.status == "login"){
						$scope.passwordPopup = "Please enter your existing password";
						$("#promptModal").modal('hide');
					}else if(data.status == "register"){
						$scope.passwordPopup = "First time login, please enter a new password to use";
						$("#promptModal").modal('hide');
					}else{
						//invalid credentials 
						$scope.error=true;
					}
					$("#loginModal").modal($scope.modalOptions);//popup password
				}
			})
			.error(function (res) {
				//401 case, user not authorized
				if(res.success){
					if(res.status == "login"){
						$("#promptModal").modal('hide');
						$scope.passwordPopup = "Please enter your existing password";
						$("#loginModal").modal($scope.modalOptions);//popup password
					}
					
				}
			});
		}else{
			$('#deviceID').focus();
		}
		
	}
	$scope.login=function (password) {
		if(password && password.trim() != ""){
			Files.login({password:password.trim(),deviceID:$scope.deviceID})
			.success(function (data) {
				if(data.status == "valid"){
					$("#loginModal").modal('hide');
					$scope.valid_input = true;
					//list files
					$scope.listFiles();
				}else{
					$scope.error=true;
				}
			})
			.error(function (res) {
				if(res.success){
					if(res.status == "invalid"){
						//invalid credentials 
						$scope.error=true;
					}
				}
			});
		}else{
			$('#password').focus();
		}
	}
	$scope.listFiles = function () {
		Files.list('deviceid='+$scope.deviceID)
					.success(function (data) {
						$scope.filesLoaded = true;
						$scope.files = data.files;
					})
					.error(function () {
						console.error("error occured");
					});
	}
	$scope.viewImage = function (data) {
		$scope.isImage = true;
		$scope.imageSource="data:image/jpeg;base64,"+arrayBufferToBase64(data)
	}

	$scope.viewPdf = function (data) {
		$scope.isImage = false;
		$scope.pdfDoc = data;
		
		$('#fileModal').on('shown.bs.modal', function() {
	        setTimeout(function() {
	        	$scope.fit();
	        },25);
	    });
	}

	$scope.print=function () {
		window.print();
	}
	function arrayBufferToBase64( buffer ) {
	    var binary = '';
	    var bytes = new Uint8Array( buffer );
	    var len = bytes.byteLength;
	    for (var i = 0; i < len; ++i) {
	        binary += String.fromCharCode( bytes[ i ] );
	    }
	    return window.btoa( binary );
	}
}]);